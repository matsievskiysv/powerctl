[[_TOC_]]
# Installation

# Program installation

1. Install requirements
	```bash
	sudo apt update
	sudo apt upgrade
	sudo apt install -y python3 python3-virtualenv
	```
1. Install package
	* Base package only
		```bash
		pip install .
		```
	* Base package with serial bus support
		```bash
		pip install .[serial]
		```
	* Base package with GPIO bus support
		```bash
		pip install .[gpio]
		```
	* Base package with all extra busses
		```bash
		pip install .[serial,gpio]
		```
1. Copy configuration file to `/etc/powerctl_<name>.yaml` where `<name>` is an instance name. Some examples of configuration files are present in `examples` directory.
1. Copy `powerctl@.service` from `extra` directory to `/etc/systemd/system/powerctl` and adjust `User` and path to `powerctl` files.
1. Start and enable `<name>` instance service
	```bash
	systemctl start powerctl@<name>
	systemctl enable powerctl@<name>
	```

## Configuration file

Server is configured using file `config.yaml`. Its required fields are different for different drivers.

### Common fields

| Configuration section | Type  | Description  |
|:--|:--|:--|
| `title`  | string  | Page title |
| `server_port`  | number  | Server port number |
| `logging`  | string  | Debug level. Allowed values: debug, info, warning, error, critical |

### Common driver fields

| Configuration section | Type  | Description  |
|:--|:--|:--|
| `driver.name`  | string  | Name of the driver used |
| `page`  | string  | HTML page used for the driver. Most drivers only allow using one page |
| `socket_count`  | number  | Number of sockets in system |

### Serial bus driver fields

Some drivers inherit common serial bus functionality. They require the following fields to be present.

| Configuration section | Type  | Description  |
|:--|:--|:--|
| `driver.port`  | string  | Path to serial port |
| `driver.baudrate`  | number  | Serial port baud rate |
| `driver.parity`  | string  | Serial port parity. Allowed values: none, even, odd, mark, space |
| `driver.byte_size`  | number  | Serial port byte size. Allowed values: 5, 6, 7, 8 |
| `driver.stop_bits`  | number  | Serial port stop bits. Allowed values: 1, 1.5, 2 |
| `driver.flow_control`  | string  | Serial port flow control. Allowed values: none, software, `rts_cts`, `dsr_dtr` |
| `driver.flow_control`  | string  | Serial port flow control. Allowed values: none, software, `rts_cts`, `dsr_dtr` |
| `driver.delay`  | number  | Serial port send byte delay. |
| `driver.timeout.write`  | number  | Serial port write timeout |
| `driver.timeout.read`  | number  | Serial port read timeout |

### GPIO driver fields

Some drivers inherit common GPIO bus functionality. They require the following fields to be present.

| Configuration section | Type  | Description  |
|:--|:--|:--|
| `driver.port`  | string  | Path to GPIO chip port |
| `driver.pins`  | array  | Array defining system pins |
| `driver.pins[].pin`  | number  | GPIO pin number |
| `driver.pins[].direction`  | string  | GPIO pin direction. Allowed values: input, output |
| `driver.pins[].inverted`  | boolean  | GPIO pin inversion |

### `zup` driver fields

`zup` driver requires serial bus fields to be present. Example configuration is in `examples/zup.yaml`.

| Configuration section | Type  | Description  |
|:--|:--|:--|
| `driver.socket_count`  | constant  | Must be 1 |
| `driver.page`  | constant  | Must be `single_voltage.html` |
| `driver.address`  | number  | Device address. Must be between 1 and 31 |
| `driver.baudrate`  | number  | Driver only supports baud rate values: 300, 600, 1200, 2400, 4800, 9600 |
| `driver.max_voltage`  | number  | Maximum voltage allowed to be set |

### `arduino_switch` driver fields

`arduino_switch` driver requires serial bus fields to be present. Example configuration is in `examples/arduino_switch.yaml`. Arduino firmware file is in `extra/arduino_switch.c` file.

| Configuration section | Type  | Description  |
|:--|:--|:--|
| `driver.page`  | constant  | Must be `multi_switch.html` |
| `driver.baudrate`  | constant  | Driver only supports baud rate 9600 |

### `gpio_switch` driver fields

`gpio_switch` driver requires GPIO bus fields to be present. Example configuration is in `examples/gpio_switch.yaml`.

| Configuration section | Type  | Description  |
|:--|:--|:--|
| `driver.page`  | constant  | Must be `multi_switch.html` |
