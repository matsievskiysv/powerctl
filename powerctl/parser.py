from jsonschema import validate
from pathlib import Path
from yaml import safe_load
import sys

from .schema import CONFIG_SCHEMA


def load_config(path: Path) -> dict:
    with open(path) as f:
        obj = safe_load(f)
    validate(obj, CONFIG_SCHEMA)
    driver_name: str = obj["driver"]["name"]
    driver_class: str = f"powerctl.drivers.{driver_name}.driver"
    __import__(driver_class)
    driver = sys.modules[driver_class].Driver
    return obj, driver(obj)
