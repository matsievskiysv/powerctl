from typing import Tuple
from json import loads as json_decode
from jsonschema import validate

from ..driver import DriverInterface
from .schema import SCHEMA


class Driver(DriverInterface):
    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)
        self._sockets = [False] * self._driver_config["socket_count"]

    def driver_info(self) -> str:
        return super().driver_info() + " " + self._driver_config["info"]

    def socket_index(self, index: int) -> str:
        return f"D{index + 1}"

    def get_socket_status(self, index: int) -> bool:
        return self._sockets[index]

    def set_socket_status(self, index: int, status: bool):
        self._sockets[index] = status

    def get_custom_call(self, args: dict) -> Tuple[str, int]:
        return {"args": args}, 200

    def post_custom_call(self, args: dict, data: bytes) -> Tuple[str, int]:
        return {"args": args, "data": json_decode(data)}, 200
