SCHEMA = {
    "type": "object",
    "required": ["info", "page"],
    "properties": {
        "info": {
            "type": "string",
            "minLength": 3,
            "maxLength": 15,
        },
        "page": {
            "const": "simple.html",
        }
    },
}
