SCHEMA = {
    "type": "object",
    "required": ["port", "pins"],
    "properties": {
        "port": {
            "type": "string",
        },
        "pins": {
            "type": "array",
            "items": {
                "type": "object",
                "required": ["pin", "direction"],
                "properties": {
                    "pin": {
                        "type": "integer",
                        "minimum": 0,
                    },
                    "direction": {
                        "enum": ["input", "output"],
                    },
                    "inverted": {
                        "type": "boolean",
                    },
                },
            },
        },
    },
}
