from jsonschema import validate
from time import sleep
import serial
import logging

from .driver import DriverInterface
from .serial_button_schema import SCHEMA


class DriverSerialToggleButton(DriverInterface):
    """Driver extension for virtual button, implemented via serial DTR signal"""
    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)
        self._ser = serial.Serial(
            port=self._driver_config["port"],
            dsrdtr=True,
            exclusive=True
        )
        self.set(False)

    def set(self, state: bool):
        logging.debug(f"button set state {state}")
        self._current_state = state ^ self._driver_config["invert_polarity"]
        self._ser.dtr = self._current_state

    def on(self):
        self.set(True)

    def off(self):
        self.set(False)

    def current_state(self) -> bool:
        return self._current_state

    def toggle_cycle(self):
        self.off()
        sleep(self._driver_config["delay"])
        self.on()
        sleep(self._driver_config["delay"])
        self.off()

    def driver_info(self) -> str:
        return f"serial port {self._driver_config['port']}"
