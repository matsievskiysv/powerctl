SCHEMA = {
    "type": "object",
    "required": ["socket_count", "page", "address", "baudrate", "max_voltage"],
    "properties": {
        "socket_count": {
            "const": 1,
        },
        "page": {
            "const": "single_voltage.html",
        },
        "address": {
            "type": "integer",
            "minimum": 1,
            "maximum": 31,
        },
        "baudrate": {
            "enum": [300, 600, 1200, 2400, 4800, 9600],
        },
        "max_voltage": {
            "type": "number",
            "minimum": 0,
        },
    }
}
