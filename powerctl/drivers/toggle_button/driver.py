from typing import Tuple
from json import dumps as json_encode
from jsonschema import validate

from ..serial_button import DriverSerialToggleButton
from .schema import SCHEMA


class Driver(DriverSerialToggleButton):
    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)
        self._selected = 0

    def driver_info(self) -> str:
        return super().driver_info()

    def get_socket_status(self, index: int) -> bool:
        return self._selected == index

    def set_socket_status(self, index: int, status: bool):
        if status:
            for _ in range((index + self.socket_count() if (index < self._selected) else index) - self._selected):
                self.toggle_cycle()
            self._selected = index

    def post_custom_call(self, args: dict, data: bytes) -> Tuple[str, int]:
        if args.get("reset_index", None) is not None:
            self._selected = 0
            return json_encode("ok"), 200
        return json_encode("unknown argument"), 400
