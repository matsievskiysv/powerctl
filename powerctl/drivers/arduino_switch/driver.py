from typing import Tuple
from json import dumps as json_encode
from jsonschema import validate
from time import sleep

from ..serial_bus import DriverSerial
from .schema import SCHEMA


class Driver(DriverSerial):

    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)
        self.term = b"\r\n"
        # align communication
        for _ in range(4):
            self.write(b"?")
            if len(self.read_until(self.term)) != 0:
                break
            sleep(0.1)
        self.write(b"??")
        rv = self.read_until(self.term)
        if len(rv) == 0:
            raise RuntimeError("No response from device")
        socket_count = int(rv.decode().strip())
        if socket_count < self._driver_config["socket_count"]:
            raise RuntimeError(f"Requested {self._driver_config['socket_count']} sockets. \
Device only has {socket_count}")

    def driver_info(self) -> str:
        return f"Arduino switch (port {self._driver_config['port']})"

    def get_socket_status(self, index: int) -> bool:
        self.write(f"?{index}".encode())
        rv = self.read_until(self.term)
        if len(rv) == 0:
            raise RuntimeError("No response from device")
        return int(rv.decode().strip()) == 1

    def set_socket_status(self, index: int, status: bool):
        self.write(f"{'^' if status else 'v'}{index}".encode())
        rv = self.read_until(self.term)
        if len(rv) == 0:
            raise RuntimeError("No response from device")
        if int(rv.decode().strip()) == 1 != status:
            raise RuntimeError("Cannot set socket")
