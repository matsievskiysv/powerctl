SCHEMA = {
    "type": "object",
    "required": ["socket_count", "page", "baudrate"],
    "properties": {
        "page": {
            "const": "multi_switch.html",
        },
        "baudrate": {
            "const": 9600,
        },
    }
}
