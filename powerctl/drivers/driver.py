from typing import Tuple
from jsonschema import validate
from threading import Lock

from .driver_schema import SCHEMA


class DriverInterface:
    """Driver interface"""
    def __init__(self, global_config: dict):
        self._global_config: dict = global_config
        self._driver_config: dict = global_config["driver"]
        validate(self._driver_config, SCHEMA)
        self.lock = Lock()

    def __enter__(self):
        self.lock.acquire()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.lock.release()

    def driver_info(self) -> str:
        return ""

    def socket_count(self) -> int:
        return self._driver_config["socket_count"]

    def socket_index(self, index: int) -> str:
        return str(index + 1)

    def get_socket_status(self, index: int) -> bool:
        return False

    def set_socket_status(self, index: int, status: bool):
        pass

    def get_custom_call(self, args: dict) -> Tuple[str, int]:
        return "", 200

    def post_custom_call(self, args: dict, data: bytes) -> Tuple[str, int]:
        return "", 200
