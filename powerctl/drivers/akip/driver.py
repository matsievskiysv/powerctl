from typing import Tuple
from json import dumps as json_encode
from jsonschema import validate
from time import sleep

from ..serial_bus import DriverSerial
from .schema import SCHEMA


class Driver(DriverSerial):
    REPEAT = 5

    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)
        self.term = b"\r\n"
        self.model = None
        for _ in range(self.REPEAT):
            self.write(b":*IDN?;" + self.term)
            rv = self.read_until(self.term)
            if len(rv) != 0:
                self.model = rv.decode().strip()
                if "AKIP" not in self.model:
                    raise RuntimeError("Unexpected device model")
                return
            sleep(0.1)
        raise RuntimeError("No response from device")

    def driver_info(self) -> str:
        return f"{self.model} (port {self._driver_config['port']})"

    def get_socket_status(self, index: int) -> bool:
        for _ in range(self.REPEAT):
            self.write(b":OUT?;" + self.term)
            rv = self.read_until(self.term)
            if len(rv) != 0:
                out = rv.decode().strip()
                if out == "ON":
                    return True
                elif out == "OFF":
                    return False
                else:
                    raise RuntimeError("Unexpected response")
            sleep(0.1)
        raise RuntimeError("No response from device")

    def set_socket_status(self, index: int, status: bool):
        for _ in range(self.REPEAT):
            self.write(f":OUT {'ON' if status else 'OFF'};{self.term}".encode())
            if self.get_socket_status(index) == status:
                return
            sleep(0.1)
        raise RuntimeError("No response from device")

    def get_custom_call(self, args: dict) -> Tuple[str, int]:
        if args.get("max_voltage", None) is not None:
            return json_encode(self._driver_config["max_voltage"]), 200
        elif args.get("programmed_voltage", None) is not None:
            return json_encode(self.get_float(b":VOLT?;" + self.term, "")), 200
        elif args.get("actual_voltage", None) is not None:
            return json_encode(self.get_float(b":VOUT?;" + self.term, "")), 200
        elif args.get("actual_current", None) is not None:
            return json_encode(self.get_float(b":IOUT?;" + self.term, "")), 200
        elif args.get("capabilities", None) is not None:
            return json_encode({"voltage_measure": True, "current_measure": True}), 200
        return json_encode("unknown argument"), 400

    def post_custom_call(self, args: dict, data: bytes) -> Tuple[str, int]:
        if args.get("voltage", None) is not None:
            try:
                value = float(args.get("voltage", None))
            except ValueError:
                return json_encode("invalid voltage value"), 400
            if value > self._driver_config["max_voltage"]:
                return "voltage exceeds maximum value", 400
            return {"target": "voltage", "value": self.set_voltage(value)}, 200
        return json_encode("unknown argument"), 400

    def get_float(self, query: bytes, prefix: str) -> float:
        for _ in range(self.REPEAT):
            self.write(query)
            rv = self.read_until(self.term)
            if len(rv) != 0:
                out = rv.decode().strip()
                if out[:len(prefix)] == prefix:
                    try:
                        value = float(out[len(prefix):])
                        return value
                    except ValueError:
                        pass
            sleep(0.1)
        raise RuntimeError("No response from device")

    def set_voltage(self, value: float) -> float:
        for _ in range(self.REPEAT):
            self.write(f":VSET {value:0.3f};{self.term}".encode())
            try:
                programmed = self.get_float(b":VOLT?;" + self.term, "")
                if abs(value - programmed) <= 0.1:
                    return programmed
            except RuntimeError:
                pass
            sleep(0.1)
        raise RuntimeError("Cannot set voltage")
