SCHEMA = {
    "type": "object",
    "required": ["socket_count", "page", "baudrate", "max_voltage"],
    "properties": {
        "socket_count": {
            "const": 1,
        },
        "page": {
            "const": "single_voltage.html",
        },
        "baudrate": {
            "const": 57600,
        },
        "max_voltage": {
            "type": "number",
            "minimum": 0,
        },
    }
}
