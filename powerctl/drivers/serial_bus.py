from jsonschema import validate
from time import sleep
import serial
import struct
import logging

from .driver import DriverInterface
from .serial_bus_schema import SCHEMA


BYTE_SIZE_MAP = {
    5: serial.FIVEBITS,
    6: serial.SIXBITS,
    7: serial.SEVENBITS,
    8: serial.EIGHTBITS,
}

PARITY_MAP = {
    "none": serial.PARITY_NONE,
    "even": serial.PARITY_EVEN,
    "odd": serial.PARITY_ODD,
    "mark": serial.PARITY_MARK,
    "space": serial.PARITY_SPACE,
}


STOP_BITS_MAP = {
    1: serial.STOPBITS_ONE,
    1.5: serial.STOPBITS_ONE_POINT_FIVE,
    2: serial.STOPBITS_TWO,
}


class DriverSerial(DriverInterface):
    """Driver extension for serial bus"""
    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)
        self._ser = serial.Serial(
            port=self._driver_config["port"],
            baudrate=self._driver_config["baudrate"],
            bytesize=BYTE_SIZE_MAP[self._driver_config["byte_size"]],
            parity=PARITY_MAP[self._driver_config["parity"]],
            stopbits=STOP_BITS_MAP[self._driver_config["stop_bits"]],
            timeout=self._driver_config["timeout"]["read"],
            write_timeout=self._driver_config["timeout"]["write"],
            xonxoff=self._driver_config["flow_control"] == "software",
            rtscts=self._driver_config["flow_control"] == "rts_cts",
            dsrdtr=self._driver_config["flow_control"] == "dsr_dtr",
            exclusive=True
        )

    def write(self, msg: bytes):
        logging.debug(f"serial write {msg}")
        for bte in msg:
            self._ser.write(struct.pack("B", bte))
            sleep(self._driver_config["delay"])
            self._ser.flush()

    def read_all(self) -> bytes:
        msg = self._ser.read_all()
        logging.debug(f"serial read {msg}")
        return msg

    def read_until(self, terminator: bytes) -> bytes:
        msg = self._ser.read_until(expected=terminator)
        logging.debug(f"serial read {msg}")
        return msg

    def read(self, count: int) -> bytes:
        try:
            msg = self._ser.read(count)
            logging.debug(f"serial read {msg}")
            return msg
        except serial.SerialTimeoutException:
            logging.debug("serial read timeout")
            return b""

    def driver_info(self) -> str:
        return f"serial port {self._driver_config['port']}"
