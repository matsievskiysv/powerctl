SCHEMA = {
    "type": "object",
    "required": ["name", "page", "socket_count"],
    "properties": {
        "name": {
            "type": "string",
            "pattern": r"^[a-zA-Z]\w+$",
        },
        "page": {
            "type": "string",
            "pattern": r"^[a-zA-Z]\w+\.html?$",
        },
        "socket_count": {
            "type": "integer",
            "minimum": 1,
        },
    }
}
