from jsonschema import validate
import gpiod
from gpiod.line import Direction, Value
import logging

from .driver import DriverInterface
from .gpio_bus_schema import SCHEMA


DIRECTION_MAP = {
    "input": Direction.INPUT,
    "output": Direction.OUTPUT,
}


def to_value(pin_obj: dict, value: bool) -> Value:
    inv = pin_obj.get("inverted", False)
    if inv:
        value = not value
    return Value.ACTIVE if value else Value.INACTIVE


def from_value(pin_obj: dict, value: Value) -> bool:
    inv = pin_obj.get("inverted", False)
    val = value == Value.ACTIVE
    if inv:
        val = not val
    return val


class DriverGPIO(DriverInterface):
    """Driver extension for GPIO bus"""
    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)
        if self._driver_config["socket_count"] > len(self._driver_config["pins"]):
            raise RuntimeError(f"Requested {self._driver_config['socket_count']} sockets. \
Defined only {len(self._driver_config['pins'])} pins")
        chip = gpiod.Chip(self._driver_config["port"])
        self.lines = chip.request_lines({pin["pin"]:
                                         gpiod.LineSettings(direction=DIRECTION_MAP[pin["direction"]],
                                                            output_value=to_value(pin, False))
                                         for pin in self._driver_config["pins"]})

    def set(self, index: int, value: bool):
        pin = self._driver_config["pins"][index]
        if pin["direction"] != "output":
            raise RuntimeError(f"gpio pin {index} is not configured as output")
        logging.debug(f"gpio set {index} to {value}")
        self.lines.set_value(pin["pin"], to_value(pin, value))

    def get(self, index: int) -> bool:
        pin = self._driver_config["pins"][index]
        rv = from_value(pin, self.lines.get_value(pin["pin"]))
        logging.debug(f"gpio get {index} is {rv}")
        return rv

    def driver_info(self) -> str:
        return f"GPIO chip {self._driver_config['port']}"
