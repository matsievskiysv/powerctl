SCHEMA = {
    "type": "object",
    "required": ["port", "delay", "invert_polarity"],
    "properties": {
        "port": {
            "type": "string",
        },
        "delay": {
            "type": "number",
            "minimum": 0.001,
        },
        "invert_polarity": {
            "type": "boolean",
        },
    },
}
