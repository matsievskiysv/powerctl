from jsonschema import validate

from ..gpio_bus import DriverGPIO
from .schema import SCHEMA


class Driver(DriverGPIO):

    def __init__(self, global_config: dict):
        super().__init__(global_config)
        validate(self._driver_config, SCHEMA)

    def driver_info(self) -> str:
        return f"GPIO switch (port {self._driver_config['port']})"

    def get_socket_status(self, index: int) -> bool:
        return self.get(index)

    def set_socket_status(self, index: int, status: bool):
        self.set(index, status)
