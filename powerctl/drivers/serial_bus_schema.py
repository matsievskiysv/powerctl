SCHEMA = {
    "type": "object",
    "required": ["port", "baudrate", "parity", "byte_size", "stop_bits",
                 "flow_control", "timeout", "delay"],
    "properties": {
        "port": {
            "type": "string",
        },
        "baudrate": {
            "type": "integer",
            "multipleOf": 60,
            "minimum": 60,
        },
        "parity": {
            "enum": ["none", "even", "odd", "mark", "space"],
        },
        "byte_size": {
            "enum": [5, 6, 7, 8],
        },
        "stop_bits": {
            "enum": [1, 1.5, 2],
        },
        "flow_control": {
            "enum": ["none", "software", "rts_cts", "dsr_dtr"],
        },
        "timeout": {
            "type": "object",
            "required": ["write", "read"],
            "properties": {
                "write": {
                    "type": "number",
                    "minimum": 0.001,
                },
                "read": {
                    "type": "number",
                    "minimum": 0.001,
                },
            }
        },
        "delay": {
            "type": "number",
            "minimum": 0.001,
        },
    },
}
