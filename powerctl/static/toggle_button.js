import { createApp } from "/static/libs/vue.esm-browser.js";
import { connect_websocket, debounce } from "/static/common.js";

createApp({
	data() {
		return {
			connected: false,
			error_message: "",
			title: "",
			description: "",
			driver_info: "",
			socket_count: 0,
			socket_info: [],
		};
	},
	created() {
		connect_websocket(
			() => {
				this.connected = true;
			},
			(event) => {
				const message = JSON.parse(event.data);
				switch (message.target) {
					case "socket": {
						this.socket_info.forEach(info => info.status = false);
						this.socket_info[message.index].status = message.value;
						break;
					}
					case "comment": {
						this.socket_info[message.index].comment = message.value;
						break;
					}
				}
			},
			() => {
				this.connected = false;
			},
		);
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				this.error_message = "";
				fetch("/api/title").then(res => {
					if (res.ok)
						res.json().then(title => this.title = title);
					else
						this.error_message = "Cannot get page title";
				});
				fetch("/api/description").then(res => {
					if (res.ok)
						res.json().then(description => this.description = description);
					else
						this.error_message = "Cannot get page description";
				});
				fetch("/api/driver_info").then(res => {
					if (res.ok)
						res.json().then(driver_info => this.driver_info = driver_info);
					else
						this.error_message = "Cannot get driver info";
				});
				fetch("/api/socket_count").then(res => {
					if (res.ok)
						res.json().then(socket_count => this.socket_count = socket_count);
					else
						this.error_message = "Cannot get socket count";
				});
			} else {
				this.error_message = "Connection lost";
				this.socket_count = 0;
			}
		},
		socket_count(new_val, old_val) {
			if (new_val == old_val)
				return;
			fetch("/api/sockets")
				.then(res => {
					if (res.ok)
						res.json().then(data => this.socket_info = data);
					else
						this.error_message = `Cannot get sockets info`;
				});
		},
	},
	methods: {
		set_socket(event, index) {
			debounce(() => {
				const query = new URLSearchParams({ index: index, value: event.target.checked });
				fetch("/api/socket_status?" + query.toString(), { method: "POST" }).then(res => {
					if (res.ok)
						this.error_message = "Socket selected";
					else
						this.error_message = "Error selecting socket";
				});
			}, `socket${index}`, 500);
		},
		set_comment(event, index) {
			debounce(() => {
				const query = new URLSearchParams({ index: index });
				const headers = new Headers({ "Content-Type": "application/json" });
				fetch("/api/comment?" + query.toString(), {
					method: "POST",
					headers: headers,
					body: JSON.stringify({ comment: event.target.value }),
				}).then(res => {
					if (res.ok)
						this.error_message = "Comment updated";
					else
						this.error_message = "Error updating comment";
				});
			}, `comment${index}`, 3000);
		},
		reset_index(event, index) {
			debounce(() => {
				const headers = new Headers({ "Content-Type": "application/json" });
				fetch("/api/custom?reset_index", {
					method: "POST",
					headers: headers,
				}).then(res => {
					if (res.ok) {
						this.error_message = "Index reset";
						fetch("/api/sockets")
							.then(res => {
								if (res.ok)
									res.json().then(data => this.socket_info = data);
								else
									this.error_message = `Cannot get sockets info`;
							});
					} else {
						this.error_message = "Error resetting index";
					}
				});
			}, "reset_index", 3000);
		},
	},
}).mount("#app");
