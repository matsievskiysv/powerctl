import { createApp } from "/static/libs/vue.esm-browser.js";

createApp({
	data() {
		return {
			error_message: "",
			binary_selected: false,
			timer: 0,
			status: "",
			error_status: "",
			error_short: "",
			error_long: "",
		};
	},
	created() {
		this.update_status();
		setInterval(() => { this.update_status(); }, 1000);
	},
	methods: {
		change_binary(event) {
			this.binary_selected = event.target.value.length > 0;
		},
		upload_binary() {
			const binary = this.$refs.binary;
			if (binary.value.length == 0)
				return;

			const query = new URLSearchParams({ filename: binary.files[0].name });
			fetch(`/.api/bootstrap/upload?${query.toString()}`, {
				headers: {
					"Content-Type": "application/octet-stream"
				},
				method: "PUT",
				body: binary.files[0]
			}).then(res => {
				if (res.ok) {
					this.error_message = "Файл загружен";
				} else {
					this.error_message = "Ошибка загрузки";
				}
			});
		},
		download_binary() {
			fetch("/.api/bootstrap/exist").then(res => {
				if (res.ok) {
					fetch("/.api/bootstrap/filename").then(res => {
						if (res.ok) {
							res.text().then(filename => {
								const anchor = document.createElement("a");
								anchor.href = `/.api/bootstrap/download`;
								anchor.download = filename;
								document.body.appendChild(anchor);
								anchor.click();
								document.body.removeChild(anchor);
							});
						} else {
							this.error_message = "Прошивка не загружена";
						}
					});
				} else {
					this.error_message = "Прошивка не загружена";
				}
			});
		},
		burn() {
			this.clear();
			fetch(`/.api/bootstrap/burn`, {
				method: "POST",
			}).then(res => {
				if (!res.ok)
					this.error_message = "Ошибка загрузки";
			});
		},
		kill() {
			fetch("/.api/kill", { method: "POST" }).then(res => {
				if (!res.ok)
					res.text().then(message => this.error_message = message);
			});
		},
		clear() {
			fetch("/.api/status", { method: "DELETE" }).then(res => {
				if (res.ok) {
					this.error_short = "";
					this.error_long = "";
				} else {
					res.text().then(message => this.error_message = message);
				}
			});
		},
		update_status() {
			fetch("/.api/status")
				.then(res => {
					if (!res.ok)
						return;
					res.text().then(data => {
						this.status = data;
						fetch("/.api/run_time")
							.then(res => {
								if (res.ok)
									res.json().then(data => this.timer = data);
							});
						if (data != "RUNNING") {
							fetch("/.api/stdout")
								.then(res => {
									if (res.ok)
									    res.text().then(data =>
										this.error_short = data);
								});
							fetch("/.api/stderr")
								.then(res => {
									if (res.ok)
									    res.text().then(data =>
										this.error_long = data);
								});
						}
						switch (data) {
							case "RUNNING":
								this.error_status = "выполняется";
								break;
							case "TERMINATED":
							case "KILLED":
								this.error_status = "отменена";
								break;
							case "FAILED":
								this.error_status = "окончена с ошибкой";
								break;
							case "FAILED_TO_START":
								this.error_status = "не запустилась";
								break;
							case "SUCCEEDED":
								this.error_status = "выполнена";
								break;
						}
					});
				});
		},
	},
}).mount("#app_bootstrap");
