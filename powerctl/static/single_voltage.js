import { createApp } from "/static/libs/vue.esm-browser.js";
import { connect_websocket, debounce } from "/static/common.js";

createApp({
	data() {
		return {
			connected: false,
			error_message: "",
			title: "",
			description: "",
			driver_info: "",
			socket_enabled: false,
			voltage: 0,
			voltage_max: 0,
			voltage_actual: 0,
			current_actual: 0,
			voltage_measure: false,
			current_measure: false,
			comment: "",
		};
	},
	created() {
		connect_websocket(
			() => {
				this.connected = true;
			},
			(event) => {
				const message = JSON.parse(event.data);
				switch (message.target) {
					case "socket": {
						if (message.index == 0) {
							console.log(message.value);
							this.socket_enabled = message.value;
							this.update_actual();
						}
						break;
					}
					case "comment": {
						if (message.index == 0)
							this.comment = message.value;
						break;
					}
					case "custom": {
						const custom_message = message.value;
						switch (custom_message.target) {
							case "voltage":
								this.voltage = custom_message.value;
								this.update_actual();
								break;
						}
						break;
					}
				}
			},
			() => {
				this.connected = false;
			},
		);
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				this.error_message = "";
				fetch("/api/title").then(res => {
					if (res.ok)
						res.json().then(title => this.title = title);
					else
						this.error_message = "Cannot get page title";
				});
				fetch("/api/description").then(res => {
					if (res.ok)
						res.json().then(description => this.description = description);
					else
						this.error_message = "Cannot get page description";
				});
				fetch("/api/driver_info").then(res => {
					if (res.ok)
						res.json().then(driver_info => this.driver_info = driver_info);
					else
						this.error_message = "Cannot get driver info";
				});
				{
					const query = new URLSearchParams({ index: 0 });
					fetch("/api/socket_status?" + query.toString()).then(res => {
						if (res.ok)
							res.json().then(value => this.socket_enabled = value);
						else
							this.error_message = "Cannot get source status";
					});
					fetch("/api/comment?" + query.toString()).then(res => {
						if (res.ok)
							res.json().then(value => this.comment = value);
						else
							this.error_message = "Cannot get comment";
					});
				}
				fetch("/api/custom?max_voltage").then(res => {
					if (res.ok)
						res.json().then(value => this.voltage_max = value);
					else
						this.error_message = "Cannot get maximum voltage";
				});
				fetch("/api/custom?programmed_voltage").then(res => {
					if (res.ok)
						res.json().then(value => this.voltage = value);
					else
						this.error_message = "Cannot get voltage";
				});
				fetch("/api/custom?capabilities").then(res => {
					if (res.ok) {
						res.json().then(caps => {
							this.voltage_measure = caps.voltage_measure;
							this.current_measure = caps.current_measure;
						});
					} else {
						this.error_message = "Cannot get capabilities";
					}
				});
			} else {
				this.error_message = "Connection lost";
			}
		},
		voltage(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val > this.voltage_max) {
				this.$nextTick(() => {
					this.voltage = old_val;
				});
				return;
			}
		},
		voltage_measure() {
			this.update_actual();
		},
		current_measure() {
			this.update_actual();
		},
	},
	methods: {
		set_socket(_) {
			const value = !this.socket_enabled;
			debounce(() => {
				const query = new URLSearchParams({ index: 0, value: value });
				fetch("/api/socket_status?" + query.toString(), { method: "POST" }).then(res => {
					if (res.ok)
						this.error_message = "Socket " + (value ? "enabled" : "disabled");
					else
						this.error_message = "Error setting socket";
				});
			}, "socket", 500);
		},
		set_comment(_) {
			debounce(() => {
				const query = new URLSearchParams({ index: 0 });
				const headers = new Headers({ "Content-Type": "application/json" });
				fetch("/api/comment?" + query.toString(), {
					method: "POST",
					headers: headers,
					body: JSON.stringify({ comment: this.comment }),
				}).then(res => {
					if (res.ok)
						this.error_message = "Comment updated";
					else
						this.error_message = "Error updating comment";
				});
			}, "comment", 3000);
		},
		update_voltage(_) {
			debounce(() => {
				const query = new URLSearchParams({ voltage: this.voltage });
				fetch("/api/custom?" + query.toString(), { method: "POST" }).then(res => {
					if (res.ok) {
						this.error_message = "Voltage updated";
					} else {
						this.error_message = "Error setting voltage";
					}
				});
			}, "voltage", 1000);
		},
		update_actual(_) {
			if (this.voltage_measure)
				fetch("/api/custom?actual_voltage").then(res => {
					if (res.ok)
						res.json().then(value => this.voltage_actual = value);
					else
						this.error_message = "Cannot get voltage measurement";
				});
			if (this.current_measure)
				fetch("/api/custom?actual_current").then(res => {
					if (res.ok)
						res.json().then(value => this.current_actual = value);
					else
						this.error_message = "Cannot get current measurement";
				});
		},
	},
}).mount("#app");
