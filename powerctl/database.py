import sqlite3
from pathlib import Path


class CommentDatabase:
    """Comment database"""
    def __init__(self, path: Path):
        self.__con = sqlite3.connect(path, check_same_thread=False)
        cur = self.__con.cursor()
        cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='comments';")
        if cur.fetchone() is None:
            cur.execute("CREATE TABLE comments(idx INTEGER PRIMARY KEY, comment TEXT);")
        self.__con.commit()

    def get_comment(self, index: int) -> str:
        cur = self.__con.cursor()
        cur.execute("SELECT comment FROM comments WHERE idx=?;", (index,))
        rv = cur.fetchone()
        return "" if rv is None else rv[0]

    def put_comment(self, index: int, comment: str) -> None:
        cur = self.__con.cursor()
        try:
            cur.execute("INSERT INTO comments VALUES (?, ?);", (index, comment))
        except sqlite3.IntegrityError:
            cur.execute("UPDATE comments SET comment=? where idx=?;", (comment, index))
        self.__con.commit()
