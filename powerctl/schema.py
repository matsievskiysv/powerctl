CONFIG_SCHEMA = {
    "type": "object",
    "required": ["title", "driver", "server_port", "logging"],
    "properties": {
        "title": {
            "type": "string",
            "minLength": 3,
            "maxLength": 15,
        },
        "description": {
            "type": "string",
            "minLength": 3,
            "maxLength": 200,
        },
        "server_port": {
            "type": "integer",
            "minimum": 1,
            "maximum": 65535,
        },
        "logging": {
            "enum": ["debug", "info", "warning", "error", "critical"],
        }

    }
}
