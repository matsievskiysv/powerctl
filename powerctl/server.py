"""PowerCTL.

Usage:
  powerctl --config <config>
  powerctl (-h | --help)
  powerctl --version

Options:
  -h --help          Show this screen.
  --version          Show version.
  --config <config>  Configuration file.
"""

from docopt import docopt
from flask import Flask, send_from_directory, redirect, request
from flask_sock import Sock
from functools import wraps
from json import dumps as json_encode
from pathlib import Path
from time import sleep
from typing import Optional
import logging
import os

from .version import version
from .parser import load_config
from .database import CommentDatabase
from .drivers import DriverInterface


LOG_LEVEL_MAP = {
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warning": logging.WARNING,
    "error": logging.ERROR,
    "critical": logging.CRITICAL,
}


app = Flask(__name__)
sock = Sock(app)
config: Optional[dict] = None
driver: Optional[DriverInterface] = None
comment_db: Optional[CommentDatabase] = None
ws_cnt: bool = True
ws_message: str = ""


def xdg_cache_home():
    if os.name == "posix":
        home = Path(os.environ["HOME"])
        return Path(os.environ.get("XDG_CACHE_HOME", home / ".cache"))
    else:
        raise NotImplementedError("Not yet supported")


def try_execute(func):
    @wraps(func)
    def inner():
        try:
            rv, rc = func()
            return rv, rc
        except Exception as e:
            return json_encode(str(e)), 500
    return inner


def ws_broadcast(message: str):
    global ws_message
    global ws_cnt
    ws_message = message
    ws_cnt = not ws_cnt


def check_index(func):
    @wraps(func)
    def inner():
        idx = request.args.get("index", None)
        if idx is None:
            return json_encode("Index not specified"), 400
        try:
            idx = int(idx)
        except ValueError:
            return json_encode("Index not numeric"), 400
        idx_max = config["driver"]["socket_count"]
        if idx < 0 or idx >= idx_max:
            return json_encode(f"Index out of range [0:{idx_max-1}]"), 400
        return func()
    return inner


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(Path(app.root_path) / "static", "favicon.ico")


@app.route("/", methods=["GET"])
def main_page():
    return redirect("/index.html", code=302)


@app.route("/index.html", methods=["GET"])
def index_page():
    return send_from_directory(Path(app.root_path) / "static", config["driver"]["page"])


@app.route("/api/title", methods=["GET"])
def title():
    return json_encode(config["title"]), 200


@app.route("/api/description", methods=["GET"])
def description():
    return json_encode(config["description"]), 200


@app.route("/api/driver_info", methods=["GET"])
@try_execute
def driver_info():
    with driver as drv:
        return json_encode(drv.driver_info()), 200


@app.route("/api/socket_count", methods=["GET"])
@try_execute
def socket_count():
    with driver as drv:
        return json_encode(drv.socket_count()), 200


@app.route("/api/sockets", methods=["GET"])
@try_execute
def get_sockets():
    data: list = [{}] * config["driver"]["socket_count"]
    with driver as drv:
        for idx in range(config["driver"]["socket_count"]):
            data[idx] = {"index": drv.socket_index(idx),
                         "status": drv.get_socket_status(idx),
                         "comment": comment_db.get_comment(idx)}
    return json_encode(data), 200


@app.route("/api/socket_index", methods=["GET"])
@check_index
@try_execute
def socket_index():
    idx = int(request.args.get("index", 0))
    with driver as drv:
        return json_encode(drv.socket_index(idx)), 200


@app.route("/api/socket_status", methods=["GET"])
@check_index
@try_execute
def get_socket_status():
    idx = int(request.args.get("index", 0))
    with driver as drv:
        return json_encode(drv.get_socket_status(idx)), 200


@app.route("/api/socket_status", methods=["POST"])
@check_index
@try_execute
def post_socket_status():
    idx = int(request.args.get("index", None))
    value = request.args.get("value", None)
    if value is None:
        return "`value` argument not specified", 400
    value = value.lower()
    if value != "true" and value != "false":
        return "`value` must be equal to `true` or `false`", 400
    with driver as drv:
        drv.set_socket_status(idx, value == "true")
    ws_broadcast(json_encode({"target": "socket",
                              "index": idx,
                              "value": value == "true"}))
    return "ok", 200


@app.route("/api/comment", methods=["GET"])
@check_index
@try_execute
def get_comment():
    idx = int(request.args.get("index", None))
    comment: str = comment_db.get_comment(idx)
    return json_encode(comment), 200


@app.route("/api/comment", methods=["POST"])
@check_index
@try_execute
def post_comment():
    idx = int(request.args.get("index", None))
    comment = request.get_json()["comment"]
    if comment is None:
        return "`comment` argument not specified", 400
    comment_db.put_comment(idx, comment)
    ws_broadcast(json_encode({"target": "comment",
                              "index": idx,
                              "value": comment}))
    return "ok", 200


@app.route("/api/custom", methods=["GET"])
@try_execute
def get_custom():
    with driver as drv:
        return drv.get_custom_call(request.args)


@app.route("/api/custom", methods=["POST"])
@try_execute
def post_custom():
    with driver as drv:
        rv, rc = drv.post_custom_call(request.args, request.data)
    if rc == 200:
        ws_broadcast(json_encode({"target": "custom", "value": rv}))
    return json_encode(rv), rc


@sock.route('/')
def ws_notify(ws):
    previous = ws_cnt
    while True:
        if ws_cnt != previous:
            ws.send(ws_message)
            previous = ws_cnt
        sleep(0.1)


def main():
    global config
    global driver
    global comment_db
    arguments = docopt(__doc__, version=version)
    config_file = Path(arguments["--config"])
    if not config_file.is_file():
        exit(f"{config_file} is not a file")
    logging.warning(f"using config file {config_file.resolve()}")
    db_dir = xdg_cache_home() / "powerctl"
    db_dir.mkdir(parents=True, exist_ok=True)
    db_file = db_dir / (config_file.name.split(".")[0] + ".sqlite3")
    logging.warning(f"using db file {db_file.resolve()}")
    config, driver = load_config(config_file.resolve())
    comment_db = CommentDatabase(db_file)
    logging.getLogger().setLevel(LOG_LEVEL_MAP[config["logging"]])
    logging.getLogger("werkzeug").setLevel(LOG_LEVEL_MAP[config["logging"]])
    app.run(host="0.0.0.0", port=config["server_port"], debug=False)
