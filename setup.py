from setuptools import setup, find_packages
from codecs import open
from pathlib import Path
from powerctl.version import version


here = Path(__file__).parent.absolute()

# Get the long description from the README file
with open(here / "README.md", encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="powerctl",
    version=version,
    author="S.V. Matsievskiy",
    author_email="matsievskiysv@gmail.com",
    maintainer="S.V. Matsievskiy",
    maintainer_email="matsievskiysv@gmail.com",
    url="https://gitlab.com/matsievskiysv/powerctl",
    description="Control panel for voltage sources",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GPLv3+",
    python_requires=">=3.8",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
        "Operating System :: OS Independent",
        "Framework :: Flask",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
        "Topic :: Scientific/Engineering",
    ],
    keywords="voltage source control",
    packages=find_packages(
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"]
    ),
    include_package_data=True,
    install_requires=["docopt~=0.6",
                      "Flask~=3.0",
                      "flask-sock~=0.7",
                      "jsonschema==4.17.3",
                      "PyYAML~=6.0"],
    extras_require={"serial": ["pyserial~=3.5"],
                    "gpio": ["gpiod~=2.1"]},
    entry_points={
        "console_scripts": ["powerctl = powerctl.server:main"]
    }
)
