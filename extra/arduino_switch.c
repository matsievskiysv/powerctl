#define ARRAY_SZ(x) (sizeof(x) / sizeof(x[0]))
#define ON LOW
#define OFF HIGH

struct {
        int pin;
        int val;
} relay_map[] = {{2, 0}, {3, 0}, {4, 0}, {5, 0},
                 {6, 0}, {7, 0}, {8, 0}, {9, 0}};

void setup() {
        for (int i = 0; i < ARRAY_SZ(relay_map); i++) {
                pinMode(relay_map[i].pin, OUTPUT);
                digitalWrite(relay_map[i].pin, relay_map[i].val ? ON : OFF);
        }
        Serial.begin(9600);
}

void loop() {
        if (Serial.available() > 0) {
                int incomingByte = Serial.read();
                if (incomingByte == -1)
                        return;

                if (incomingByte == '?') {
                        while (Serial.available() == 0)
                                ;
                        int incomingByte = Serial.read();
                        if (incomingByte == -1)
                                return;
                        if (incomingByte == '?') {
                                Serial.println(ARRAY_SZ(relay_map));
                        } else if ((incomingByte >= '0') && (incomingByte <= '9')) {
                                if (incomingByte - '0' >= ARRAY_SZ(relay_map)) {
                                        Serial.println("?");
                                } else {
                                        Serial.println(relay_map[incomingByte - '0'].val);
                                }
                        } else {
                                Serial.println("?");
                        }
                } else if ((incomingByte == '^') || (incomingByte == 'v')) {
                        int value = incomingByte == '^';
                        while (Serial.available() == 0)
                                ;
                        int incomingByte = Serial.read();
                        if (incomingByte == -1)
                                return;
                        if ((incomingByte >= '0') && (incomingByte <= '9')) {
                                if (incomingByte - '0' >= ARRAY_SZ(relay_map)) {
                                        Serial.println("?");
                                } else {
                                        relay_map[incomingByte - '0'].val = value;
                                        digitalWrite(relay_map[incomingByte - '0'].pin, value ? ON : OFF);
                                        Serial.println(value);
                                }
                        }
                } else {
                        Serial.println("?");
                }
        }
}
